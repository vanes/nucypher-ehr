# This is an example of Alice setting a Policy on the NuCypher network.
# In this example, Alice uses n=3.

# WIP w/ hendrix@3.1.0

import binascii
import datetime
import logging
import shutil
import sys
import os
import maya
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from nucypher.characters.lawful import Alice, Bob, Ursula
from nucypher.config.characters import AliceConfiguration
from nucypher.data_sources import DataSource
# This is already running in another process.
from nucypher.network.middleware import RestMiddleware
from umbral.keys import UmbralPublicKey

##
# Boring setup stuff.
##

from flask import Flask
from flask import request, render_template, redirect, url_for
app = Flask(__name__)

import re
import json

global login
login = "Alice"

def hlthvctr_updt(date, section, new_data):
    local_json = json.loads(new_data)
    with open(sys.argv[1], mode='r+') as feedsjson:
        feeds = json.load(feedsjson)
        if feeds["Healthvector"][0]["Date"] == date:
            if section == "Activities":
                if local_json["Points"] == "10":
                    feeds["Healthvector"][0]["Sleep"] = str(int(feeds["Healthvector"][0]["Sleep"]) + 40)
                elif local_json["Points"] == "50":
                    feeds["Healthvector"][0]["Sport"] = str(int(feeds["Healthvector"][0]["Sport"]) + 35)
                else:
                    feeds["Healthvector"][0]["Sport"] = str(int(feeds["Healthvector"][0]["Sport"]) + 10)
            else: 
                if local_json["Fast food"] != "0":
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) - 10)
                elif local_json["Grow food"] != "0":
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) + 20)
                elif local_json["Alcohol"] != "0":
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) - 15)
                else:
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) + 10) 
        else: 
            feeds["Healthvector"][0]["Date"] = date
            feeds["Healthvector"][0]["Sleep"] = "0"
            feeds["Healthvector"][0]["Sport"] = "0"
            feeds["Healthvector"][0]["Food"] = "30"
            feeds["Healthvector"][0]["Temperature"] = "100"
            feeds["Healthvector"][0]["Analises"] = "100"
            if section == "Activities":
                if local_json["Points"] == "10":
                    feeds["Healthvector"][0]["Sleep"] = str(int(feeds["Healthvector"][0]["Sleep"]) + 40)
                elif local_json["Points"] == "50":
                    feeds["Healthvector"][0]["Sport"] = str(int(feeds["Healthvector"][0]["Sport"]) + 35)
                else:
                    feeds["Healthvector"][0]["Sport"] = str(int(feeds["Healthvector"][0]["Sport"]) + 10)
            else: 
                if local_json["Fast food"] != "0":
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) - 10)
                elif local_json["Grow food"] != "0":
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) + 20)
                elif local_json["Alcohol"] != "0":
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) - 15)
                else:
                    feeds["Healthvector"][0]["Food"] = str(int(feeds["Healthvector"][0]["Food"]) + 10)
        feedsjson.seek(0)
        feedsjson.write(json.dumps(feeds))
        feedsjson.truncate()
    return 1


#Editdata page
@app.route("/editdata")
def editdata():
    path = 'aehr.json'
    with open(path, 'r') as f:
       data = json.loads(f.read())       
    print (data) 
    return render_template('editdata.html', parsed_string=data, login=login)

#Activity page   
@app.route("/activity")
def activity():
    path = 'aehr.json'
    with open(path, 'r') as f:
       data = json.loads(f.read())       
    print (data) 
    return render_template('activity.html', parsed_string=data, login=login)   
   
#Action for form "Add history" (add new data to json-file) 
@app.route("/addhistory", methods=['POST'])
def addhistory():
    date = request.form['Date']
    doctor =  request.form['Doctor']
    procedure = request.form['Procedure']
    remark = request.form['Remark']
    #with open(sys.argv[1], 'r') as rf:
    #    json_data = json.load(f)
    with open(sys.argv[1], mode='r') as feedsjson:
        feeds = json.load(feedsjson)
    print (feeds)
    for record in feeds["History"]:
        num = int(record['Num'])
    with open(sys.argv[1], mode='w') as feedsjson:
        json.dump([], feedsjson)    
    with open(sys.argv[1], mode='w') as feedsjson:
        history = {'Num': str(num + 1), 'Procedure': procedure, 'Date': date, 'Doctor': doctor, 'Remark': remark}
        feeds["History"].append(history)
        json.dump(feeds, feedsjson)
    #for record in json_data["History"]:
    #    num = record['Num']
    #with open('my_file.json', 'w') as wf:
    #    history = {}
    #    history['Num'] = num
    #   history['Date'] = date
    #    history['Doctor'] = doctor
    #    history['Procedure'] = procedure
    #    history['Remark'] = remark
    #    json.dump(history, wf)
    print (json.dumps({'Date': date,'Doctor':doctor,'Procedure':procedure}))
    return json.dumps({'Date': date,'Doctor':doctor,'Procedure':procedure})

#Action for form "Add food" (update food statistics in json-file) 
@app.route("/addfood", methods=['POST'])
def addfood():
    date = request.form['Date'] 
    r_form = request.form
    if 'Fast food' in r_form.keys():
        fast_food = request.form['Fast food']
        to_hlthvctr = """{"Fast food":"10", "Grow food":"0", "Alcohol":"0", "Drinks":"0"}"""
    else:
        fast_food = 0
    if 'Grow food' in r_form.keys():
        grow_food = request.form['Grow food']
        to_hlthvctr = """{"Fast food":"0", "Grow food":"10", "Alcohol":"0", "Drinks":"0"}"""
    else:
        grow_food = 0
    if 'Alcohol' in r_form.keys():
        alcohol = request.form['Alcohol']
        to_hlthvctr = """{"Fast food":"0", "Grow food":"0", "Alcohol":"10", "Drinks":"0"}"""
    else:
        alcohol = 0        
    if 'Drinks' in r_form.keys():
        drinks = request.form['Drinks']
        to_hlthvctr = """{"Fast food":"0", "Grow food":"0", "Alcohol":"0", "Drinks":"10"}"""
    else:
        drinks = 0
    hlthvctr_updt(date, "Food", to_hlthvctr)
    with open(sys.argv[1], mode='r') as feedsjson:
        feeds = json.load(feedsjson)
    is_include = 0
    cnt = 0
    for record in feeds["Food"]:
        if record["Date"] == date:
            is_include = 1
            break
        else:
            cnt = cnt + 1
    if is_include == 0:
        print ("FOOD NOT INCLUDE")
        food_init = {'Date': date, 'Fast food':'0', 'Grow food':'0', 'Alcohol':'0', 'Drinks':'0'}
        with open(sys.argv[1], mode='w') as feedsjson:
            json.dump([], feedsjson) 
        with open(sys.argv[1], mode='w') as feedsjson:
            food_init["Fast food"] = str(int(food_init["Fast food"]) + int(fast_food))
            food_init["Grow food"] = str(int(food_init["Grow food"]) + int(grow_food))
            food_init["Alcohol"] = str(int(food_init["Alcohol"]) + int(alcohol))
            food_init["Drinks"] = str(int(food_init["Drinks"]) + int(drinks))
            feeds["Food"].append(food_init)
            print(feeds["Food"])
            json.dump(feeds, feedsjson)
            feedsjson.close()
    else:
        print ("FOOD INCLUDE" + str(cnt))
        with open(sys.argv[1], mode='r+') as feedsjson:
            feeds = json.load(feedsjson)
            feeds["Food"][cnt]["Fast food"] = str(int(feeds["Food"][cnt]["Fast food"]) + int(fast_food))
            feeds["Food"][cnt]["Grow food"] = str(int(feeds["Food"][cnt]["Grow food"]) + int(grow_food))
            feeds["Food"][cnt]["Alcohol"] = str(int(feeds["Food"][cnt]["Alcohol"]) + int(alcohol))
            feeds["Food"][cnt]["Drinks"] = str(int(feeds["Food"][cnt]["Drinks"]) + int(drinks))
            feedsjson.seek(0)
            feedsjson.write(json.dumps(feeds))
            feedsjson.truncate()
    print (json.dumps({'Date':date, 'Fast food': fast_food,'Grow food':grow_food,'Alcohol':alcohol, 'Drinks':drinks}))
    return json.dumps({'Date':date, 'Fast food': fast_food,'Grow food':grow_food,'Alcohol':alcohol, 'Drinks':drinks})

#Action for form "Add activity" (update activity statistics in json-file)     
@app.route("/addactivity", methods=['POST'])
def addactivity():
    date = request.form['Date']
    time = request.form['Time']
    activity = request.form['Activity']
    to_hlthvctr = """{"Points":\""""+activity+""""}"""
    with open(sys.argv[1], mode='r') as feedsjson:
        feeds = json.load(feedsjson)
    is_include = 0
    cnt = 0
    for record in feeds["Activity"]:
        if record["Date"] == date:
            is_include = 1
            break
        else:
            cnt = cnt + 1
    if is_include == 0:
        print ("ACTIVITY NOT INCLUDE")
        activity_init = {'Date': date, '08:00':'0', '12:00':'0', '16:00':'0', '20:00':'0', '0:00':'0'}
        with open(sys.argv[1], mode='w') as feedsjson:
            json.dump([], feedsjson) 
        with open(sys.argv[1], mode='w') as feedsjson:
            activity_init = {'Date': date, '08:00':'0', '12:00':'0', '16:00':'0', '20:00':'0', '0:00':'0'}
            activity_init[time] = str(int(activity_init[time]) + int(activity))
            feeds["Activity"].append(activity_init)
            print(feeds["Activity"])
            json.dump(feeds, feedsjson)
            feedsjson.close()
    else:
        print ("ACTIVITY INCLUDE" + str(cnt))
        with open(sys.argv[1], mode='r+') as feedsjson:
            feeds = json.load(feedsjson)
            feeds["Activity"][cnt][time] = str(int(feeds["Activity"][cnt][time]) + int(activity))
            feedsjson.seek(0)
            feedsjson.write(json.dumps(feeds))
            feedsjson.truncate()
    print (json.dumps({'Date':date, 'Time':time, 'Activity':activity}))
    hlthvctr_updt(date, "Activities", to_hlthvctr)
    return json.dumps({'Date':date, 'Time':time, 'Activity':activity})

#Action for form "Add blood test result" (add new data to json-file)
@app.route("/addanalises", methods=['POST'])
def addanalises():
    rd_bld_cll = request.form['Red blood cell']
    hemoglobin =  request.form['Hemoglobin']
    hematocrit = request.form['Hematocrit']
    wht_bld_cll_cnt = request.form['White blood cell count']
    pltlt_cnt = request.form['Platelet count']
    date = request.form['Date']
    with open(sys.argv[1], mode='r') as feedsjson:
        feeds = json.load(feedsjson)
    print (feeds)
    with open(sys.argv[1], mode='w') as feedsjson:
        json.dump([], feedsjson)    
    with open(sys.argv[1], mode='w') as feedsjson:
        history = {'Date': date, 'Red blood cell': rd_bld_cll, 'Hemoglobin': hemoglobin, 'Hematocrit': hematocrit, 'White blood cell count': wht_bld_cll_cnt, 'Platelet count': pltlt_cnt}
        feeds["Blood test"].append(history)
        json.dump(feeds, feedsjson)
    return json.dumps({'Date': date,'Hemoglobin':hemoglobin,'Hematocrit':hematocrit})

#Profile page
@app.route("/profile")
def finnegans_wake_func():
    print (login)
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    root.addHandler(ch)
    
    teacher_rest_port = sys.argv[2]
    with open("examples-runtime-cruft/node-metadata-{}".format(teacher_rest_port), "r") as f:
        f.seek(0)
        teacher_bytes = binascii.unhexlify(f.read())
    URSULA = Ursula.from_bytes(teacher_bytes, federated_only=True)
    print("Will learn from {}".format(URSULA))
    
    SHARED_CRUFTSPACE = "{}/examples-runtime-cruft".format(os.path.dirname(os.path.abspath(__file__)))
    CRUFTSPACE = "{}/finnegans-wake-demo".format(SHARED_CRUFTSPACE)
    CERTIFICATE_DIR = "{}/certs".format(CRUFTSPACE)
    shutil.rmtree(CRUFTSPACE, ignore_errors=True)
    os.mkdir(CRUFTSPACE)
    os.mkdir(CERTIFICATE_DIR)
    
    URSULA.save_certificate_to_disk(CERTIFICATE_DIR)
    
    #########
    # Alice #
    #########
    
    
    ALICE = Alice(network_middleware=RestMiddleware(),
                  known_nodes=(URSULA,),
                  federated_only=True,
                  #always_be_learning=True,
                  known_certificates_dir=CERTIFICATE_DIR,
                  )
    
    # Here are our Policy details.
    policy_end_datetime = maya.now() + datetime.timedelta(days=5)
    m = 1
    n = 2
    label = b"secret/files/and/stuff"
    
    # Alice grants to Bob.
    BOB = Bob(known_nodes=(URSULA,),
              federated_only=True,
              #always_be_learning=True,
              known_certificates_dir=CERTIFICATE_DIR)
    ALICE.start_learning_loop(now=True)
    policy = ALICE.grant(BOB, label, m=m, n=n,
                         expiration=policy_end_datetime)
    
    # Alice puts her public key somewhere for Bob to find later...
    alices_pubkey_bytes_saved_for_posterity = bytes(ALICE.stamp)
    
    # ...and then disappears from the internet.
    del ALICE
    # (this is optional of course - she may wish to remain in order to create
    # new policies in the future.  The point is - she is no longer obligated.
    
    #####################
    # some time passes. #
    # ...               #
    #                   #
    # ...               #
    # And now for Bob.  #
    #####################
    
    # Bob wants to join the policy so that he can receive any future
    # data shared on it.
    # He needs a few pieces of knowledge to do that.
    BOB.join_policy(label,  # The label - he needs to know what data he's after.
                    alices_pubkey_bytes_saved_for_posterity,  # To verify the signature, he'll need Alice's public key.
                    # He can also bootstrap himself onto the network more quickly
                    # by providing a list of known nodes at this time.
                    node_list=[("localhost", 3601)]
                    )
    
    # Now that Bob has joined the Policy, let's show how DataSources
    # can share data with the members of this Policy and then how Bob retrieves it.
    finnegans_wake = open(sys.argv[1], 'rb')
    
    # We'll also keep track of some metadata to gauge performance.
    # You can safely ignore from here until...
    ################################################################################
    
    start_time = datetime.datetime.now()
    json_string = """"""
    for counter, plaintext in enumerate(finnegans_wake):
        if counter % 20 == 0:
            now_time = datetime.datetime.now()
            time_delta = now_time - start_time
            seconds = time_delta.total_seconds()
            print("********************************")
            print("Performed {} PREs".format(counter))
            print("Elapsed: {}".format(time_delta.total_seconds()))
            print("PREs per second: {}".format(counter / seconds))
            print("********************************")
    
        ################################################################################
        # ...here.  OK, pay attention again.
        # Now it's time for...
    
        #####################
        # Using DataSources #
        #####################
    
        # Now Alice has set a Policy and Bob has joined it.
        # You're ready to make some DataSources and encrypt for Bob.
    
        # It may also be helpful to imagine that you have multiple Bobs,
        # multiple Labels, or both.
    
        # First we make a DataSource for this policy.
        data_source = DataSource(policy_pubkey_enc=policy.public_key)
    
        # Here's how we generate a MessageKit for the Policy.  We also get a signature
        # here, which can be passed via a side-channel (or posted somewhere public as
        # testimony) and verified if desired.  In this case, the plaintext is a
        # single passage from James Joyce's Finnegan's Wake.
        # The matter of whether encryption makes the passage more or less readable
        # is left to the reader to determine.
        message_kit, _signature = data_source.encapsulate_single_message(plaintext)
    
        # The DataSource will want to be able to be verified by Bob, so it leaves
        # its Public Key somewhere.
        data_source_public_key = bytes(data_source.stamp)
    
        # It can save the MessageKit somewhere (IPFS, etc) and then it too can
        # choose to disappear (although it may also opt to continue transmitting
        # as many messages as may be appropriate).
        del data_source
    
        ###############
        # Back to Bob #
        ###############
    
        # Bob needs to reconstruct the DataSource.
        datasource_as_understood_by_bob = DataSource.from_public_keys(
            policy_public_key=policy.public_key,
            datasource_public_key=data_source_public_key,
            label=label
        )
    
        # Now Bob can retrieve the original message.  He just needs the MessageKit
        # and the DataSource which produced it.
        alice_pubkey_restored_from_ancient_scroll = UmbralPublicKey.from_bytes(alices_pubkey_bytes_saved_for_posterity)
        delivered_cleartexts = BOB.retrieve(message_kit=message_kit,
                                            data_source=datasource_as_understood_by_bob,
                                            alice_verifying_key=alice_pubkey_restored_from_ancient_scroll)
    
        # We show that indeed this is the passage originally encrypted by the DataSource.
        assert plaintext == delivered_cleartexts[0]
        json_string = json_string + format(delivered_cleartexts[0])
    #Replace excess chars from json-string after re-encrypt (or after update json-file)
    json_string1 = re.sub(r'\s+', ' ', json_string) 
    json_string1 = re.sub(r'$|\t|\n|\r', '', json_string1) 
    print(json_string1.replace('b\'', '').replace('\'', '"')) 
    jso = json_string1.replace('b\'', '').replace('}\'', '}')
    #Convert string to json & render a template
    parsed_string = json.loads(jso) 
    return render_template('profile.html', parsed_string=parsed_string, login=login)
    
@app.route("/enter", methods=['POST'])
def enter():
    global login
    login = request.form['login']
    print (login)
    return redirect(url_for('finnegans_wake_func'))
    
@app.route("/")
def startpage():
    return render_template('enter.html')


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")    


FROM python:3.6

WORKDIR /home
COPY . /home
RUN pip3 install Flask
RUN pip3 install pipenv
RUN pipenv install --dev --three --skip-lock --system --deploy
RUN pip3 install -e .
RUN cd examples && \
    mkdir examples-runtime-cruft && \
    chmod 777 examples-runtime-cruft && \
    chmod 777 run_federated_ursula.py && \
    chmod 777 finnegans-wake-federated.py
EXPOSE 80
CMD python3 examples/run_federated_ursula.py 3500
# Электронная мед. книжка (RUS)
Электронная медицинская карта — медицинская карта пациента медучреждения в электронной форме.Такой подход хранения информации о пациенте позволяет избежать бумажной волокиты и упрощает процесс взаимодействия пациентов и врачей. Учитывая объем данных (история болезни, результаты анализов, диспансеризации и т.п.) гораздо удобнее хранить их в едином цифровом источнике, но при этом важно упростить поиск нужной информации для врачей о пациенте путем выдачи только тех данных, которые нужны в конкретном случае лечения. Например, лаборатории по сбору анализов крови не нужно знать о всей истории болезни пациента, а нужны лишь предыдущие результаты анализов для сравнения. Также как и хирурга не будет интересовать диагнозы поставленные стоматологом. Более того, все медицинские данные являются приватной информацией, к которой нужно ограничивать доступ. Это можно реализовать средствами NuCypher.  

# NuCypher 
NuCypher - децентрализованная система управления ключами (KMS). Она обеспечивает приватность данных в публичных blockchain сетях и децентрализованных приложениях.  Благодаря своим службам повторного шифрования и контроля доступа NuCypher ICO позволяет разработчикам DApps-приложений хранить, обмениваться, управлять и защищать свои данные в блочной цепочке. 

# Идея проекта - HealthVector 
Данный проект позволяет не только вести учет истории болезни и результатов анализов, но также помогает пользователю оценить свой образ жизни с точки зрения влияние на его здоровье. К примеру, здоровое питание, занятия спортом и сон оказывают благоприятное воздействие на организм человека, в то время как отсутствие сна, частое употребление алкоголя и вредная пища наоборот "подрывают" иммунитет. Healthvector представляет собой "диаграмму здоровья" человека, которую можно заполнять в игровой форме и оценивать свое состояние во время болезни и не только.  

# Реализация
Проект реализован в виде сайта. Данные о пациенте хранятся в файле формата .json (файл должен быть записан одной строкой). Хранится такая информация, как история болезни, результаты анализов крови, данные об активности пользователя в течение дня, данные о питании пользователя в течении дня, данные HealthVector. Пациент (Alice) может вносить данные об активности в течение дня (спорт, работа, сон) и видеть результат на графике активности по часам, вносить данные о питании и видеть, какая еда преобладает в его рационе, а также оценить свое состояние по диаграмме HealthVector. Преобладание здоровой еды и занятий спортом увеличивают значения HealthVector, в то время как выпивка и отсутствие активности уменьшают. Данные о результатах анализов на данном этапе HealthVector не учитываются и всегда берется значение по умолчанию (ввиду того, что я не знаю как классифицировать результаты анализов, что там хорошо, а что нет). Разграничение доступа выполняется средствами NuCypher (используется https://github.com/nucypher/nucypher). NuCypher также позволяет разграничить доступ по ключам json. Протестировать систему можно за Alice (пациент), Bob (доктор) и Lab (лаборатория).

# Средства реализации 
- Ubuntu 16.04LTS
- Python 3.7 Flask
- NuCypher lib (https://github.com/nucypher/nucypher)
- HTML, CSS, JS
- Bootstrap 4

# Как запустить
Докер - все в одном контейнере (в 3х терминалах): 
- docker build . 
- docker run --network="host" {{$image-name}} python3 examples/run_federated_ursula.py 3500 
- Нужно получитьID контейнера docker-image-id (docker ps -a) 
- docker exec -it {{$id}} python3 examples/run_federated_ursula.py 3501 3500 
- docker exec -it {{$id}} python3 examples/finnegans-wake-federated.py aehr.json 3501 
- docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' {$image name}
В браузере открыть ссылку с консоли Flask (0.0.0.0:5000)


Без докера (Linux):
- 1-й терминал
- pipenv install --dev --three --skip-lock
- pipenv shell
- pip3 install -e .
- cd examples
- python3 run_federated_ursula.py 3500  # <- seed node
- 2-й терминал
- python3 run_federated_ursula.py 3501 3500
- 3-й терминал
- python3 finnegans-wake-federated.py aehr.json 3501

# Electronic Health Records (English)
Electronic medical record - a medical record of a patient of a medical institution in electronic form. This approach to storing information about a patient allows you to avoid paperwork and simplifies the process of interaction between patients and doctors. Given the amount of data (medical history, test results, medical examinations, etc.) it is much more convenient to store them in a single digital source, but it is important to simplify the search for the necessary information for doctors about the patient by issuing only the data needed in a particular case of treatment. . For example, laboratories for collecting blood tests do not need to know about the entire patient's medical history, but only previous results of the tests are necessary for comparison. As well as the surgeon will not be interested in the diagnoses made by the dentist. Moreover, all medical data is private information that needs to be restricted. This can be implemented using NuCypher.

#Project idea - HealthVector
This project allows not only to keep records of medical history and test results, but also helps the user to assess their lifestyle in terms of the impact on his health. For example, a healthy diet, exercise and sleep have a beneficial effect on the human body, while the lack of sleep, frequent use of alcohol and junk food on the contrary "undermine" the immune system. Healthvector is a "health chart" of a person, which can be filled in the form of a game and assess your condition during illness and not only.

# Implementation
The project is implemented as a site. Patient data is stored in a .json file (the file must be written in one line). Information such as medical history, results of blood tests, data on the user's activity during the day, data on the user's nutrition during the day, HealthVector data is stored. The patient (Alice) can enter activity data during the day (sport, work, sleep) and see the result on the activity chart by the hour, enter the nutrition data and see which food is prevalent in his diet, as well as assess his condition using the HealthVector chart . The prevalence of healthy eating and playing sports increase the HealthVector, while drinking and lack of activity reduce it. Data on the test results at this stage of the HealthVector are not taken into account and the default value is always taken (due to the fact that I don’t know how to classify the test results, what’s good and what’s not). Access control is performed by means of NuCypher (used https://github.com/nucypher/nucypher). NuCypher also allows you to differentiate access by json keys. You can test the system for Alice (patient), Bob (doctor) and Lab (laboratory).

# Technology stack 
- Ubuntu 16.04LTS
- Python 3.7 Flask
- NuCypher lib (https://github.com/nucypher/nucypher)
- HTML, CSS, JS
- Bootstrap 4

# How to run demo

- docker build . 
- docker run --network="host" {{$image-name}} python3 examples/run_federated_ursula.py 3500 
Get docker-image-id (docker ps -a) 
- docker exec -it {{$id}} python3 examples/run_federated_ursula.py 3501 3500
- docker exec -it {{$id}} python3 examples/finnegans-wake-federated.py aehr.json 3501 
- docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' {$image name}
In browser:
{{IP from flask}}:5000


Without docker (Linux):
- 1st console
- pipenv install --dev --three --skip-lock
- pipenv shell
- pip3 install -e .
- cd examples
- mkdir examples-runtime-cruft
- python3 run_federated_ursula.py 3500  # <- seed node
- 2nd console
- python3 run_federated_ursula.py 3501 3500
- 3rd console
- python3 finnegans-wake-federated.py aehr.json 3501

